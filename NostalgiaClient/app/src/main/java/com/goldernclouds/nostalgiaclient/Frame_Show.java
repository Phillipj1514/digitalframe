package com.goldernclouds.nostalgiaclient;

import android.app.Activity;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import java.io.IOException;

public class Frame_Show extends AppCompatActivity implements SurfaceHolder.Callback, MediaPlayer.OnPreparedListener {
    SurfaceView frame;
    MediaPlayer player;
    SurfaceHolder holder;

    private static final String VIDEO_Path  = "https://sample-videos.com/video123/mp4/720/big_buck_bunny_720p_20mb.mp4";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.frame_show);
        hideSystemUI();

        frame = (SurfaceView)findViewById(R.id.frmae);
        holder=frame.getHolder();
        holder.addCallback(Frame_Show.this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent go = new Intent(getApplicationContext(), Main.class);
        startActivity(go);
        finish();
    }

    @Override
    protected void onPause() {
        super.onPause();
        ReleaseMediaPlayer();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ReleaseMediaPlayer();
    }

    private void ReleaseMediaPlayer() {
        if(player!= null){
            player.release();
            player = null;
        }
    }


    private void hideSystemUI() {
        // Enables regular immersive mode.
        // For "lean back" mode, remove SYSTEM_UI_FLAG_IMMERSIVE.
        // Or for "sticky immersive," replace it with SYSTEM_UI_FLAG_IMMERSIVE_STICKY
        View decorView = getWindow().getDecorView();
        decorView.setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_IMMERSIVE
                        // Set the content to appear under the system bars so that the
                        // content doesn't resize when the system bars hide and show.
                        | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        // Hide the nav bar and status bar
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        player = new MediaPlayer();
        player.setDisplay(holder);
        try{
            player.setDataSource(VIDEO_Path);
            player.prepare();
            player.setOnPreparedListener(Frame_Show.this);
            player.setAudioStreamType(AudioManager.STREAM_MUSIC);
        }catch (IOException e){

        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        player.start();
    }
}
